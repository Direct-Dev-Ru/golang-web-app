-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: mysql-db:3306
-- Время создания: Авг 13 2022 г., 04:53
-- Версия сервера: 8.0.30
-- Версия PHP: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

CREATE TABLE `Users` (
  `id` bigint UNSIGNED NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(24) DEFAULT NULL,
  `hash` varchar(1024) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(24) NOT NULL DEFAULT 'user',
  `status` varchar(24) NOT NULL DEFAULT 'pending',
  `phone` varchar(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Users`
--

INSERT INTO `Users` (`id`, `email`, `first_name`, `last_name`, `gender`, `hash`, `createdAt`, `updatedAt`, `role`, `status`, `phone`) VALUES
(8, 'boris@eng.com', 'Boris', 'Jhonson', 'male', '$2a$10$I89nmjrWzBiX2yWN7WDs/uhxeLKPT9X/QUKMzu5FcHxNpMS2aE/42', '2022-08-03 13:02:39', '2022-08-03 13:02:39', 'user', 'pending', NULL),
(219, 'zena@ravaga.com', 'Zena', 'Billy', 'female', '$2a$10$JbScvHJBFg3Cqo9oS5CL9O1avJZv/6J1BDhYKeLXtpDvtDq3uWN6m', '2022-08-03 15:47:16', '2022-08-03 15:47:16', 'user', 'pending', NULL),
(233, 'kuzant24@gmail.com', 'Anton', 'Kuznetcov', 'male', '$2a$10$1eExZ77lFo7KbY7vducaOu8PiDRGw/gZFvLozjBEnxFQuSEod5nXu', '2022-08-05 10:00:19', '2022-08-05 10:00:19', 'poweruser', 'active', '+79059809956'),
(234, 'kuznetcovay@yandex.ru', 'Anton', 'Kuznetcov', 'male', '$2a$10$hVqXNeal40JFt2DdHlyBFeXVxwyz0ZfVjNY4i1rUmGJbYlG3n0Nmy', '2022-08-06 12:39:10', '2022-08-06 12:39:10', 'admin', 'active', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Users`
--
ALTER TABLE `Users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


CREATE TABLE sessions (
    token CHAR(43) PRIMARY KEY,
    data BLOB NOT NULL,
    expiry TIMESTAMP(6) NOT NULL
);

CREATE INDEX sessions_expiry_idx ON sessions (expiry);