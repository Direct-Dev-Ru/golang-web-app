package config

import (
	"database/sql"
	"html/template"
	"log"
	"net/http"
	"time"

	db_model "github.com/Direct-Dev-Ru/simple-web-app-golang/model/db-model"
	mylog "github.com/Direct-Dev-Ru/simple-web-app-golang/utils/logs"
	"github.com/alexedwards/scs/v2"
	jsoniter "github.com/json-iterator/go"
	"github.com/rs/zerolog"
)

type AppConfig struct {
	InProd bool
}
type AppModel struct {
}

func (app *AppModel) GetNewUser() *db_model.User {
	return &db_model.User{}
}

type Application struct {
	InfoLog  *zerolog.Logger
	ErrorLog *log.Logger
	FileLog  *log.Logger
	Logger   *mylog.Logga
	Store    *db_model.Store
	// Store          *scheme.Store
	StorePath      string
	DbPool         *sql.DB
	TemplatesPath  string
	TemplatesCache *map[string]*template.Template
	AppDbWrap      *db_model.DbWrap
	AppConfig      *AppConfig
	SessionManager *scs.SessionManager
	AppModel       *AppModel
	Json           jsoniter.API
}

func NewApplication(storePath *string) *Application {
	i, e, f := mylog.AppLogger.GetAllLogs()

	app := &Application{
		InfoLog: i, ErrorLog: e, FileLog: f, Logger: &mylog.AppLogger,
		Store: nil, StorePath: *storePath, DbPool: nil,
		TemplatesPath:  "./ui/templates",
		TemplatesCache: nil,
		AppDbWrap:      nil,
		AppConfig:      &AppConfig{true},
		SessionManager: scs.New(), AppModel: &AppModel{}}
	app.SetUpSessionManager()
	app.Json = jsoniter.ConfigCompatibleWithStandardLibrary
	return app
}

func (app *Application) SetUpSessionManager() {
	app.SessionManager.Lifetime = 24 * 5 * time.Hour
	app.SessionManager.IdleTimeout = 24 * 5 * time.Hour
	app.SessionManager.Cookie.Name = "session_id"
	// app.SessionManager.Cookie.Domain = "example.com"
	app.SessionManager.Cookie.HttpOnly = true
	// app.SessionManager.Cookie.Path = "/example/"
	app.SessionManager.Cookie.Persist = true
	app.SessionManager.Cookie.SameSite = http.SameSiteStrictMode
	app.SessionManager.Cookie.Secure = app.AppConfig.InProd
}

func (app Application) GetDbPool() *sql.DB {
	return app.DbPool
}

func (app Application) GetAppLoggers() (*zerolog.Logger, *log.Logger, *log.Logger) {
	return mylog.AppLogger.GetAllLogs()
}

func (app Application) GetStorePath() *string {
	return &app.StorePath
}

func (app *Application) GetStore() *db_model.Store {
	return app.Store
}

func (app *Application) SetStore(store *db_model.Store) {
	app.Store = store
	if app.AppDbWrap != nil {
		app.AppDbWrap.SetStore(store)
		app.Store.AppDbWrap = app.AppDbWrap
	}
}
