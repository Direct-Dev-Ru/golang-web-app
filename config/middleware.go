package config

import (
	"context"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"github.com/justinas/nosurf"
)

type MyKeyString string

func (app *Application) Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, req)
		app.InfoLog.Printf("%s %s %s", req.Method, req.RequestURI, time.Since(start))
	})
}

func (app *Application) NoSurf(next http.Handler) http.Handler {
	csrfHandler := nosurf.New(next)

	csrfHandler.SetBaseCookie(http.Cookie{
		Name:     "csrf",
		HttpOnly: true,
		Path:     "/",
		Secure:   app.AppConfig.InProd,
		SameSite: http.SameSiteLaxMode,
	})

	csrfHandler.ExemptGlobs("/api/*", "/api/*/", "/api/*/*", "/api/*/*/",
		"/api/*/*/*", "/api/*/*/*/")
	// csrfHandler.ExemptPaths("/api/admin/users/")
	return csrfHandler
}

func (app *Application) IsAuthenticated(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		userJsonData := app.SessionManager.GetString(req.Context(), "UserJsonData")

		if len(userJsonData) > 0 {
			user := app.AppModel.GetNewUser()
			err := app.Json.Unmarshal([]byte(userJsonData), user)

			if err == nil && user != nil {

				isAuthenticated := true
				ctx := context.WithValue(req.Context(), MyKeyString("UserEmail"), user.Email)
				ctx = context.WithValue(ctx, MyKeyString("UserName"),
					user.First_name+" "+user.Last_name)
				ctx = context.WithValue(ctx, MyKeyString("IsAuthenticated"), isAuthenticated)

				next.ServeHTTP(w, req.WithContext(ctx))
			}
		} else {
			isAuthenticated := false
			ctx := context.WithValue(req.Context(), MyKeyString("IsAuthenticated"), isAuthenticated)
			next.ServeHTTP(w, req.WithContext(ctx))
		}
	})
}

// func (app *Application) IsAuthenticated(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
// 		authCookie, _ := req.Cookie("auth-cookie")

// 		if authCookie != nil {
// 			user, err := app.AppDbWrap.GetUserByEmail(authCookie.Value)
// 			if err == nil && user != nil {

// 				isAuthenticated := true
// 				ctx := context.WithValue(req.Context(), MyKeyString("UserEmail"), authCookie.Value)
// 				ctx = context.WithValue(ctx, MyKeyString("UserName"),
// 					user.First_name+" "+user.Last_name)
// 				ctx = context.WithValue(ctx, MyKeyString("IsAuthenticated"), isAuthenticated)
// 				// ctx = context.WithValue(ctx, MyKeyString("UserJsonData"), string(userJson))

// 				next.ServeHTTP(w, req.WithContext(ctx))
// 			}
// 		} else {
// 			isAuthenticated := false
// 			ctx := context.WithValue(req.Context(), MyKeyString("IsAuthenticated"), isAuthenticated)
// 			next.ServeHTTP(w, req.WithContext(ctx))
// 		}
// 	})
// }
