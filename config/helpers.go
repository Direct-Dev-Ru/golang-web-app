package config

import (
	"database/sql"
	"io"
	"net/http"
	"time"

	"github.com/alexedwards/scs/mysqlstore"
	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

// Помощник serverError записывает сообщение об ошибке в errorLog и
// затем отправляет пользователю ответ 500 "Внутренняя ошибка сервера".
func (app *Application) ServerError(w http.ResponseWriter, err error) {
	// trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
	app.Logger.ErrorPrintln(err.Error())

	// http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

// Помощник clientError отправляет определенный код состояния и соответствующее описание
// пользователю. Мы будем использовать это в следующий уроках, чтобы отправлять ответы вроде 400 "Bad
// Request", когда есть проблема с пользовательским запросом.
func (app *Application) ClientError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
}

// Мы также реализуем помощник notFound. Это просто
// удобная оболочка вокруг clientError, которая отправляет пользователю ответ "404 Страница не найдена".
func (app *Application) NotFound(w http.ResponseWriter) {
	app.ClientError(w, http.StatusNotFound)
}

// renderJSON renders 'v' as JSON and writes it as a response into w.
func (app *Application) RenderJSON(w http.ResponseWriter, v interface{}, wrap bool) {
	type SuccessResponse struct {
		IsError bool        `json:"iserror"`
		Error   string      `json:"error"`
		Payload interface{} `json:"payload"`
	}
	var send []byte
	var err error
	if wrap {
		send, err = app.Json.Marshal(SuccessResponse{false, "", v})
	} else {
		send, err = app.Json.Marshal(v)
	}
	// app.InfoLog.Println("From RenderJSON:", string(json))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		//app.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(send)

}

func (app *Application) RenderErrorJSON(w http.ResponseWriter, e error) {
	type ErrorResponse struct {
		IsError bool   `json:"iserror"`
		Error   string `json:"error"`
	}
	errRes := ErrorResponse{IsError: true, Error: e.Error()}
	json, err := app.Json.Marshal(errRes)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		//app.ServerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(json)
}

func (app *Application) HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	// A very simple health check.
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	// In the future we could report back on the status of our DB, or our cache
	// (e.g. Redis) by performing a simple PING, and include them in the response.
	io.WriteString(w, `{"alive": true}`)
}

// Функция openDB() обертывает sql.Open() и возвращает пул соединений sql.DB
// для заданной строки подключения (DSN).
func (app *Application) OpenDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetConnMaxIdleTime(time.Minute * 5)

	if err = db.Ping(); err != nil {
		return nil, err
	}
	app.DbPool = db
	app.SessionManager.Store = mysqlstore.New(db)
	return db, nil
}

func (app *Application) GetPwdHash(p string) (string, error) {
	bp := []byte(p)
	// Hashing the password with the default cost of 10
	hp, err := bcrypt.GenerateFromPassword(bp, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	// Comparing the password with the hash
	// err = bcrypt.CompareHashAndPassword(hashedPassword, password)
	return string(hp), nil
}
