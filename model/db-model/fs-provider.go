package db_model

import (
	"errors"
	"io/ioutil"
	"os"
)

type FSProvider struct {
	filePath string
}

func NewFSProvider(fpath string) DbProvider {
	var fs FSProvider = FSProvider{filePath: fpath}
	return DbProvider(fs)
}

func (provider FSProvider) GetProviderType() *string {
	var ProviderType = "fs"
	return &ProviderType
}

func (provider FSProvider) SelectAllUsers() (map[string]*User, error) {
	return nil, errors.New("unsupported method")
}

func (provider FSProvider) SelectUserById(id int) (*User, error) {
	return nil, errors.New("unsupported method")
}

func (provider FSProvider) SelectUserByEmail(email string) (*User, error) {
	return nil, errors.New("unsupported method")
}

func (provider FSProvider) SelectUsersByFilter(filter string, v ...interface{}) ([]*User, error) {
	return nil, errors.New("unsupported method")
}

func (provider FSProvider) InsertOneUser(u *User) (*User, error) {
	return nil, errors.New("unsupported method")
}

// File realisation

func (provider FSProvider) LoadStore() (*Store, error) {
	if provider.filePath == "" {
		return nil, errors.New("empty filePath in fs-provider")
	}

	var store Store
	file, err := os.Open(provider.filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&store)
	if err != nil {
		return nil, err
	}
	return &store, nil
}

func (provider FSProvider) SelectAllMembers() (map[string]*Member, error) {

	store, err := provider.LoadStore()
	if err != nil {
		return nil, err
	}
	mmap := make(map[string]*Member, len(store.Members))
	for _, m := range store.Members {
		mmap[m.Email] = m
	}
	return mmap, nil
}

func (provider FSProvider) SelectMemberByEmail(email string) (*Member, error) {

	mmap, err := provider.SelectAllMembers()

	if err != nil {
		return nil, err
	}
	m, ok := mmap[email]
	if !ok {
		return nil, nil
	}
	return m, nil
}

func (provider FSProvider) NextId(store *Store) int {
	max := 0
	for _, m := range store.Members {
		if m.Id > max {
			max = m.Id
		}
	}
	return max + 1
}

func (provider FSProvider) InsertOneMember(m *Member) (*Store, error) {
	store, err := provider.LoadStore()
	if err != nil {
		return nil, err
	}

	if m.Id < 0 {
		m.Id = provider.NextId(store)
	}

	exists := false
	for _, mb := range store.Members {
		if mb.Email == m.Email {
			exists = true
			break
		}
	}
	if exists {
		return nil, errors.New("method InsertOneMember in fs-provider: member exists")
	}

	store.Members = append(store.Members, m)

	return store, nil
}

func (provider FSProvider) DeleteOneMember(m *Member) (*Store, error) {
	store, err := provider.LoadStore()
	if err != nil {
		return nil, err
	}
	ms := make([]*Member, len(store.Members))
	for _, mb := range store.Members {
		if mb.Email != m.Email {
			ms = append(ms, mb)
		}
	}
	if len(ms) < len(store.Members) {
		store.Members = ms
		err = provider.SaveStore(store)
		if err != nil {
			return nil, err
		}
	}
	return store, nil
}

func (provider FSProvider) UpdateOneMember(m *Member) (*Store, error) {
	store, err := provider.LoadStore()
	if err != nil {
		return nil, err
	}
	updated := false
	for index, mb := range store.Members {
		if mb.Email == m.Email {
			m.Id = mb.Id
			store.Members[index] = m
			updated = true
			break
		}
	}
	if updated {
		err = provider.SaveStore(store)
		if err != nil {
			return nil, err
		}
	}
	return store, nil
}

func (provider FSProvider) DeleteAllMembers() error {
	if provider.filePath == "" {
		return errors.New("empty filePath in fs-provider")
	}
	fi, err := os.Lstat(provider.filePath)
	if err != nil {
		return err
	}

	file, err := os.OpenFile(provider.filePath, os.O_RDWR, fi.Mode().Perm())
	if err != nil {
		return err
	}
	defer file.Close()

	var store Store
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&store)
	if err != nil {
		return err
	}
	store.Members = []*Member{}
	err = provider.SaveStore(&store)
	return err
}

func (provider FSProvider) SaveStore(store *Store) (err error) {
	if provider.filePath == "" {
		return errors.New("empty filePath in fs-provider")
	}
	fi, err := os.Lstat(provider.filePath)
	if err != nil {
		return err
	}

	storeToWrite := struct {
		Info    *Info
		Members []*Member
	}{Info: store.Info, Members: store.Members}

	newMembersBytes, err := json.MarshalIndent(storeToWrite, "", "  ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(provider.filePath, newMembersBytes, fi.Mode().Perm())
	if err != nil {
		return err
	}

	return nil
}
