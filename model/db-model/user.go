package db_model

import (
	"errors"
	"strings"
)

var ErrNoRecord = errors.New("mysql_models: record not found")

type User struct {
	Id           int
	Email        string
	First_name   string
	Last_name    string
	Gender       string
	Phone        string
	Role         string
	Status       string
	HashPassword string
}

type UserForClient struct {
	Id         int
	Email      string
	First_name string
	Last_name  string
	Gender     string
	Phone      string
}

func NewUser(email, first_name, last_name, gender, phone, hash string) *User {
	return &User{-1, email, first_name, last_name, gender, phone, "user", "pending", hash}
}
func NewUserForClient(u *User) *UserForClient {
	return &UserForClient{u.Id, u.Email, u.First_name, u.Last_name, u.Gender, u.Phone}
}

func (db *DbWrap) GetUsers() map[string]*User {
	return db.users
}

func (db *DbWrap) SetUsers(newusersmap map[string]*User) map[string]*User {
	db.users = newusersmap
	return db.users
}

// select all users from database and save it in users field of dbwrap object
func (db *DbWrap) PopulateUsers() (map[string]*User, error) {
	users, err := db.provider.SelectAllUsers()
	if err != nil {
		db.users = nil
		return nil, err
	}
	db.users = users
	return db.users, nil
}

// select user from database by specified email
func (db *DbWrap) GetUserByEmail(email string) (*User, error) {
	return db.provider.SelectUserByEmail(email)
}

// select user from database by specified id
func (db *DbWrap) GetUserById(id int) (*User, error) {
	return db.provider.SelectUserById(id)
}

func (db *DbWrap) AppendUsers(newUsers ...*User) (map[string]*User, error) {
	var err error = nil
	mapUsers := make(map[string]*User)
	for _, user := range newUsers {

		// validation
		user.Gender = strings.ToLower(strings.TrimSpace(user.Gender))
		if user.Gender != "male" && user.Gender != "female" {
			err = errors.New("validation error: wrong Gender value")
			return nil, err
		}
		// lets insert row
		_, err = db.provider.InsertOneUser(user)
		if err != nil {
			return nil, err
		}
		mapUsers[user.Email] = user

	}
	return mapUsers, nil
}
