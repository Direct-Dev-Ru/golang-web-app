package db_model

import (
	"database/sql"
	"errors"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type MySQLProvider struct {
	dbPool *sql.DB
}

func NewMySQLProvider(dbPool *sql.DB) DbProvider {
	var mysqldb MySQLProvider = MySQLProvider{dbPool: dbPool}
	return DbProvider(mysqldb)
}

func (provider MySQLProvider) GetProviderType() *string {
	var ProviderType = "mysql"
	return &ProviderType
}

func (provider MySQLProvider) SelectAllUsers() (map[string]*User, error) {
	ret := make(map[string]*User)
	selectResult, err := provider.SelectUsersByFilter(" 1 ")
	if len(selectResult) == 0 {
		strError := "mysql_model(SelectAllUsers): empty result"
		if err != nil {
			strError += ". " + err.Error()
		}
		err = errors.New(strError)
		return nil, err
	}
	for _, u := range selectResult {
		ret[u.Email] = u
	}
	return ret, nil
}

func (provider MySQLProvider) SelectUserById(id int) (*User, error) {
	selectResult, err := provider.SelectUsersByFilter(" id = ?", id)
	if len(selectResult) == 0 {
		strError := "mysql_model: empty result"
		if err != nil {
			strError += ". " + err.Error()
		}
		err = errors.New(strError)
		return nil, err
	}
	if len(selectResult) > 1 {
		return nil, errors.New("mysql_model: more than one record returned in single row query")
	}

	return selectResult[0], nil

}

func (provider MySQLProvider) SelectUserByEmail(email string) (*User, error) {
	selectResult, err := provider.SelectUsersByFilter(" email = ?", email)
	if len(selectResult) == 0 {
		strError := "mysql_model: empty result"
		if err != nil {
			strError += ". " + err.Error()
		}
		err = errors.New(strError)
		return nil, err
	}
	if len(selectResult) > 1 {
		return nil, errors.New("mysql_model: more than one record returned in single row query")
	}

	return selectResult[0], nil

}

func (provider MySQLProvider) SelectUsersByFilter(filter string, v ...interface{}) ([]*User, error) {

	// Prepare statement for reading data from user table
	selectStmt := `SELECT 	id, email, first_name, last_name, gender, phone, role, status,
							hash, createdAt, updatedAt FROM Users `
	if filter == "" {
		filter = " 1 "
	}
	selectStmt += "WHERE" + filter
	stmtOut, err := provider.dbPool.Prepare(selectStmt)

	if err != nil {
		return nil, err
	}

	defer stmtOut.Close()

	rows, err := stmtOut.Query(v...)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var rowId int
	var rowEmail, rowFirstName, rowLastName, rowGender, rowPhone, rowRole, rowStatus, rowHash []byte
	var rowCreatedAt, rowUpdatedAt time.Time
	ret := make([]*User, 0, 3)

	for rows.Next() {
		err = rows.Scan(&rowId, &rowEmail, &rowFirstName, &rowLastName, &rowGender, &rowPhone,
			&rowRole, &rowStatus, &rowHash, &rowCreatedAt, &rowUpdatedAt)
		if err != nil {
			return nil, err
		}
		u := NewUser(string(rowEmail), string(rowFirstName), string(rowLastName),
			string(rowGender), string(rowPhone), string(rowHash))
		u.Id = rowId
		u.Role = string(rowRole)
		u.Status = string(rowStatus)
		ret = append(ret, u)
	}

	return ret, nil
}

func (provider MySQLProvider) InsertOneUser(u *User) (*User, error) {

	sqlStmt := `INSERT INTO Users (email, first_name, last_name, gender, hash)
    VALUES(?, ?, ?, ?, ?)`

	// INSERT INTO `Users` (`id`, `email`, `first_name`, `last_name`, `gender`, `hash`,
	// `createdAt`, `updatedAt`) VALUES (NULL, 'kuzant24@gmail.com', 'Anton', 'Kuznetsov',
	// 'male', 'somehash by bcrypt', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

	// Prepare statement for inserting data
	stmtIns, err := provider.dbPool.Prepare(sqlStmt)
	if err != nil {
		return nil, err
	}
	defer stmtIns.Close() // Close the statement when we leave function

	result, err := stmtIns.Exec(u.Email, u.First_name, u.Last_name, u.Gender, u.HashPassword)
	if err != nil {
		return nil, err
	}

	// We use LastInsertId() to get id of inserted row
	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	u.Id = int(id)
	return u, nil
}

func (provider MySQLProvider) SelectAllMembers() (map[string]*Member, error) {
	return nil, errors.New("unsupported method")
}
func (provider MySQLProvider) SelectMemberByEmail(email string) (*Member, error) {
	return nil, errors.New("unsupported method")
}
func (provider MySQLProvider) InsertOneMember(m *Member) (*Store, error) {
	return nil, errors.New("unsupported method")
}
func (provider MySQLProvider) DeleteOneMember(m *Member) (*Store, error) {
	return nil, errors.New("unsupported method")
}
func (provider MySQLProvider) UpdateOneMember(m *Member) (*Store, error) {
	return nil, errors.New("unsupported method")
}
func (provider MySQLProvider) DeleteAllMembers() error {
	return errors.New("unsupported method")
}
func (provider MySQLProvider) SaveStore(store *Store) error {
	return errors.New("unsupported method")
}
func (provider MySQLProvider) LoadStore() (*Store, error) {
	return nil, errors.New("unsupported method")
}
