package db_model

import (
	"database/sql"
	"log"

	"github.com/rs/zerolog"
)

type Databaser interface {
	GetDbPool() *sql.DB
	GetAppLoggers() (*zerolog.Logger, *log.Logger, *log.Logger)
	GetStorePath() *string
}

type DbProvider interface {
	GetProviderType() *string
	SelectAllUsers() (map[string]*User, error)
	SelectUserById(id int) (*User, error)
	SelectUserByEmail(email string) (*User, error)
	InsertOneUser(u *User) (*User, error)

	SelectAllMembers() (map[string]*Member, error)
	SelectMemberByEmail(email string) (*Member, error)
	InsertOneMember(m *Member) (*Store, error)
	DeleteOneMember(m *Member) (*Store, error)
	UpdateOneMember(m *Member) (*Store, error)
	DeleteAllMembers() error
	SaveStore(store *Store) error
	LoadStore() (*Store, error)
}

type DbWrap struct {
	users         map[string]*User
	store         *Store
	dbPool        *sql.DB
	provider      DbProvider
	storeProvider DbProvider
}

func NewDbWrap(app Databaser) *DbWrap {
	usersmap := make(map[string]*User)
	provider := NewMySQLProvider(app.GetDbPool())
	storeProvider := NewFSProvider(*app.GetStorePath())
	dbWrap := &DbWrap{users: usersmap, dbPool: app.GetDbPool(), provider: provider,
		store: nil, storeProvider: storeProvider}

	return dbWrap
}

func (dbwrap *DbWrap) GetDbPool() *sql.DB {
	return dbwrap.dbPool
}
func (db *DbWrap) GetStore() *Store {
	return db.store
}
func (db *DbWrap) SetStore(store *Store) {
	db.store = store
}
