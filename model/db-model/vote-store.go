package db_model

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"sync"

	jsoniter "github.com/json-iterator/go"
)

var json jsoniter.API = jsoniter.ConfigCompatibleWithStandardLibrary

type Member struct {
	Id        int    `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Comment   string `json:"comment"`
	Vote      int    `json:"vote"`
}

type Info struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

type Store struct {
	Info    *Info
	Members []*Member
	sync.Mutex
	StorePath *string
	AppDbWrap *DbWrap
}

func logError(err error, app Databaser) {
	if err != nil {
		i, e, f := app.GetAppLoggers()
		if i != nil {
			i.Print(err)
		}
		if e != nil {
			e.Println(err)
		}
		if f != nil {
			f.Println(err)
		}
	}
}

func NewStore(app Databaser) *Store {

	defaultStorePath := "./data/list.json"
	resultStorePath := defaultStorePath
	if *app.GetStorePath() != "" {
		resultStorePath = *app.GetStorePath()
	}

	var store Store
	storeFile, err := os.Stat(resultStorePath)
	if err != nil {
		if os.IsNotExist(err) {
			info := &Info{
				Title:       "Some Thing About Something",
				Description: "We are voting for something. What is it? Nobody knows ..."}
			store.Info = info
			store.Members = []*Member{}
			store.StorePath = &resultStorePath
			newJsonBytes, err := json.MarshalIndent(store, "", "  ")
			if err != nil {
				return nil
			}
			err = ioutil.WriteFile(resultStorePath, newJsonBytes, 0755)
			if err != nil {
				return nil
			}
			return &store
		}
	}

	store.Lock()
	defer store.Unlock()
	file, err := os.OpenFile(resultStorePath, os.O_RDONLY, storeFile.Mode().Perm())
	logError(err, app)
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	logError(err, app)

	json.Unmarshal(b, &store)
	store.StorePath = app.GetStorePath()

	return &store
}

func (store *Store) PrintBdList() {
	var json_data, err = json.Marshal(store)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(json_data))
}

func (store *Store) AppendMember(m *Member, save bool) (err error) {

	store.Lock()
	defer store.Unlock()
	newStore, err := store.AppDbWrap.storeProvider.InsertOneMember(m)
	if err != nil {
		return
	}
	store.Members = newStore.Members
	if save {
		err = store.AppDbWrap.storeProvider.SaveStore(store)
		return
	}
	return
}

func (store *Store) UpdateMember(m *Member, save bool) (err error) {

	store.Lock()
	defer store.Unlock()
	updStore, err := store.AppDbWrap.storeProvider.UpdateOneMember(m)
	if err != nil {
		return
	}
	store.Members = updStore.Members
	if save {
		err = store.AppDbWrap.storeProvider.SaveStore(store)
	}
	return
}

func (store *Store) GetMemberByEmail(email string) (m *Member, err error) {

	store.Lock()
	defer store.Unlock()

	if email != "" {
		m, err = store.AppDbWrap.storeProvider.SelectMemberByEmail(email)
		return
	}
	return nil, errors.New("method GetMemberByEmail: bad email")
}
