package model

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/Direct-Dev-Ru/simple-web-app-golang/config"
	scheme "github.com/Direct-Dev-Ru/simple-web-app-golang/model/scheme"
	mylog "github.com/Direct-Dev-Ru/simple-web-app-golang/utils/logs"
)

func checkError(err error) {
	if err != nil {
		mylog.AppLogger.ErrorPrintln(err)
		mylog.AppLogger.InfoLog.Println(err)
	}
}

func FindByEmail(slice []interface{}, value string) (result *scheme.Member) {
	for _, item := range slice {

		switch m := item.(type) {
		case *scheme.Member:
			if m.Email == value {
				return m
			}
		}
	}
	return nil
}

var AppData *scheme.Store

func NewStore(app *config.Application) *scheme.Store {
	appData := GetStore(app.StorePath)
	return appData
}

func GetStore(storePath string) *scheme.Store {
	defaultStorePath := "./data/list.json"
	resultStorePath := defaultStorePath
	if storePath != "" {
		resultStorePath = storePath
	}
	fmt.Println("Get Store param:", storePath)
	var store scheme.Store
	store.Lock()
	defer store.Unlock()
	file, err := os.OpenFile(resultStorePath, os.O_RDONLY, 0666)
	checkError(err)
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	checkError(err)

	json.Unmarshal(b, &store)
	store.StorePath = storePath
	PrintBdList(&store)
	return &store
}

func PrintBdList(store *scheme.Store) {
	var json_data, err = json.Marshal(store)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(json_data))
}

func AppendMemberAndSave(store *scheme.Store, m *scheme.Member) (success bool, err error) {
	//appending newMember to slice of all Members and rewrite json file
	var result bool = true
	// backupSlice := make([]*scheme.Member, len(store.Members))
	// copy(backupSlice, store.Members)
	store.Lock()
	defer store.Unlock()
	mylog.AppLogger.InfoLog.Println(*m)
	store.Members = append(store.Members, m)
	newMembersBytes, err := json.MarshalIndent(store, "", "   ")

	if err != nil {
		// store.Members = backupSlice
		return !result, err
	}

	err = ioutil.WriteFile(store.StorePath, newMembersBytes, 0666)

	if err != nil {
		// store.Members = backupSlice
		return !result, err
	}

	return result, nil
}

func AppendMember(store *scheme.Store, m *scheme.Member) {
	store.Lock()
	defer store.Unlock()
	store.Members = append(store.Members, m)

}
