package scheme

import (
	"sync"

	db "github.com/Direct-Dev-Ru/simple-web-app-golang/model/db-model"
)

type Member struct {
	Id        int    `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Comment   string `json:"comment"`
	Vote      int    `json:"vote"`
}

type Info struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

type Store struct {
	sync.Mutex
	AppDbWrap *db.DbWrap
	StorePath string
	Info      *Info
	Members   []*Member
}
