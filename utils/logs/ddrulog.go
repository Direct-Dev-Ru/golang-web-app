package ddrulog

import (
	"log"
	"os"

	"github.com/rs/zerolog"
	zlog "github.com/rs/zerolog/log"
)

var Zlog zerolog.Logger
var ZlogC zerolog.Logger
var ZlogE zerolog.Logger

var AppLogger Logga

func init() {
	// zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	Zlog := zlog.Logger
	Zlog.Print("hello Zlogger from ddrulog")

	ZlogC := zlog.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	ZlogE := zlog.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	ZlogC.Info().Timestamp().Str("foo", "bar").Msg("Hello World")
	ZlogE.Error().Timestamp().Str("foo", "bar").Msg("Hello World")
	Zlog.Print("hello 2 Zlogger from ddrulog")

	AppLogger = Logga{
		// InfoLog:  log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime),
		InfoLog:  &ZlogC,
		ErrorLog: log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Llongfile),
		FileLog:  nil,
	}

}

type Logga struct {
	InfoLog  *zerolog.Logger
	ErrorLog *log.Logger
	FileLog  *log.Logger
}

func (l *Logga) GetAllLogs() (*zerolog.Logger, *log.Logger, *log.Logger) {
	return l.InfoLog, l.ErrorLog, l.FileLog
}

func (l *Logga) SetFileLog(filename string) (*os.File, error) {
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		f.Close()
		return f, err
	}

	flog := log.New(f, "FILE-INFO\t", log.Ldate|log.Ltime|log.Llongfile)
	l.FileLog = flog
	return f, nil
}

func (l *Logga) GetFileLog() *log.Logger {
	return l.FileLog
}

func (l *Logga) InfoPrintln(s ...interface{}) {
	if l.FileLog != nil {
		l.FileLog.Println(s...)
	}
	l.InfoLog.Print(s...)
}

func (l *Logga) ErrorPrintln(s ...interface{}) {
	if l.FileLog != nil {
		l.FileLog.Println(s...)
	}
	l.ErrorLog.Println(s...)
}

func (l *Logga) ErrorPrintf(formatString string, v ...interface{}) {
	if l.FileLog != nil {
		l.FileLog.Printf("-- ErrorLog -- "+formatString, v...)
	}
	l.ErrorLog.Printf(formatString, v...)
}

func (l *Logga) InfoPrintf(formatString string, v ...interface{}) {
	if l.FileLog != nil {
		l.FileLog.Printf(formatString, v...)
	}
	l.InfoLog.Printf(formatString, v...)
}
