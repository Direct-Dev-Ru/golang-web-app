package handlers

import (
	"net/http"
	"strings"

	"github.com/Direct-Dev-Ru/simple-web-app-golang/config"
	"github.com/justinas/nosurf"
	"golang.org/x/crypto/bcrypt"
)

// LogIn handler
func LogInHandler(app *config.Application) http.HandlerFunc {

	return func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Access-Control-Allow-Origin", "*")
		if request.Method == http.MethodOptions {
			return
		}
		csrfToken := nosurf.Token(request)
		if request.Method == http.MethodGet {
			err := TemplatesCache["login.page"].Execute(writer, struct {
				IsAuthenticated bool
				Email           string
				Errors          []string
				Token           string
			}{
				false,
				"",
				[]string{},
				csrfToken,
			})
			if err != nil {
				app.Logger.ErrorPrintln("Error while processing template login.page", err)
				http.Error(writer, http.StatusText(http.StatusInternalServerError)+err.Error(),
					http.StatusInternalServerError)
				return
			}
		} else if request.Method == http.MethodPost {
			// "POST" METHOD PROCESSING
			request.ParseForm()

			signInEmails, okEmail := request.Form["email"]
			signInPwds, okPwd := request.Form["password"]

			signInEmail := strings.TrimSpace(signInEmails[0])
			signInPwd := strings.TrimSpace(signInPwds[0])
			// check fields from client
			errors := []string{}
			if !(okEmail && okPwd) {
				errors = append(errors, "form fields is missing (email or password)")
			}
			if signInEmail == "" {
				errors = append(errors, "You need enter email address as your account")
			}
			if signInPwd == "" {
				errors = append(errors, "You need enter password")
			}

			if len(errors) > 0 {
				TemplatesCache["login.page"].Execute(writer,
					struct {
						Errors          []string
						Email           string
						IsAuthenticated bool
						Token           string
					}{
						Email:           signInEmail,
						Errors:          errors,
						IsAuthenticated: false,
						Token:           csrfToken,
					})
			} else {
				// check user in database
				users, err := app.AppDbWrap.PopulateUsers()
				if err != nil {
					http.Error(writer, http.StatusText(http.StatusInternalServerError)+err.Error(),
						http.StatusInternalServerError)
					return
				}
				user, ok := users[signInEmail]
				if ok {
					// Comparing the password with the hash
					err = bcrypt.CompareHashAndPassword([]byte(user.HashPassword), []byte(signInPwd))
					if err != nil {
						// Password incorrect
						TemplatesCache["login.page"].Execute(writer,
							struct {
								Errors          []string
								Email           string
								IsAuthenticated bool
								Token           string
							}{
								Errors:          []string{"wrong password"},
								Email:           signInEmail,
								IsAuthenticated: false,
								Token:           csrfToken,
							})
					}

					// cookie := &http.Cookie{
					// 	Name:     "auth-cookie",
					// 	Value:    signInEmail,
					// 	MaxAge:   60 * 60 * 24 * 7,
					// 	Path:     "/",
					// 	HttpOnly: true,
					// 	Secure:   app.AppConfig_InProd,
					// 	SameSite: http.SameSiteLaxMode,
					// }
					// http.SetCookie(writer, cookie)

					//set session-data UserJson key
					userJson, err := app.Json.Marshal(user)

					if err != nil {
						http.Error(writer, http.StatusText(http.StatusInternalServerError)+err.Error(),
							http.StatusInternalServerError)
						return
					}
					app.SessionManager.Put(request.Context(), "UserJsonData", string(userJson))

					http.Redirect(writer, request, "/", http.StatusSeeOther)
				} else {
					http.Redirect(writer, request, "/register", http.StatusTemporaryRedirect)
				}
			}
		} else {
			http.Error(writer, http.ErrBodyNotAllowed.Error(), http.StatusMethodNotAllowed)
		}
	}
}

// SignUp handler
func SignUpHandler(app *config.Application) http.HandlerFunc {

	return func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Access-Control-Allow-Origin", "*")
		if request.Method == http.MethodOptions {
			return
		}
		csrfToken := nosurf.Token(request)
		if request.Method == http.MethodGet {
			err := TemplatesCache["register.page"].Execute(writer, struct {
				IsAuthenticated bool
				Email           string
				Errors          []string
				Token           string
			}{
				false,
				"",
				[]string{},
				csrfToken,
			})
			if err != nil {
				app.Logger.ErrorPrintln("Error while processing template register.page", err)
				http.Error(writer, http.StatusText(http.StatusInternalServerError)+err.Error(),
					http.StatusInternalServerError)
				return
			}
		} else if request.Method == http.MethodPost {
			// "POST" METHOD PROCESSING
			request.ParseForm()

			signInEmails, okEmail := request.Form["email"]
			signInPwds, okPwd := request.Form["password"]

			signInEmail := strings.TrimSpace(signInEmails[0])
			signInPwd := strings.TrimSpace(signInPwds[0])
			// check fields from client
			errors := []string{}
			if !(okEmail && okPwd) {
				errors = append(errors, "form fields is missing (email or password)")
			}
			if signInEmail == "" {
				errors = append(errors, "You need enter email address as your account")
			}
			if signInPwd == "" {
				errors = append(errors, "You need enter password")
			}

			if len(errors) > 0 {
				TemplatesCache["login.page"].Execute(writer,
					struct {
						Errors          []string
						Email           string
						IsAuthenticated bool
						Token           string
					}{
						Email:           signInEmail,
						Errors:          errors,
						IsAuthenticated: false,
						Token:           csrfToken,
					})
			} else {
				// check user in database
				users, err := app.AppDbWrap.PopulateUsers()
				if err != nil {
					http.Error(writer, http.StatusText(http.StatusInternalServerError)+err.Error(),
						http.StatusInternalServerError)
					return
				}
				user, ok := users[signInEmail]
				if ok {
					// Comparing the password with the hash
					err = bcrypt.CompareHashAndPassword([]byte(user.HashPassword), []byte(signInPwd))
					if err != nil {
						// Password incorrect
						TemplatesCache["login.page"].Execute(writer,
							struct {
								Errors          []string
								Email           string
								IsAuthenticated bool
								Token           string
							}{
								Errors:          []string{"wrong password"},
								Email:           signInEmail,
								IsAuthenticated: false,
								Token:           csrfToken,
							})
					}

					// cookie := &http.Cookie{
					// 	Name:     "auth-cookie",
					// 	Value:    signInEmail,
					// 	MaxAge:   60 * 60 * 24 * 7,
					// 	Path:     "/",
					// 	HttpOnly: true,
					// 	Secure:   app.AppConfig_InProd,
					// 	SameSite: http.SameSiteLaxMode,
					// }
					// http.SetCookie(writer, cookie)

					//set session-data UserJson key
					userJson, err := app.Json.Marshal(user)

					if err != nil {
						http.Error(writer, http.StatusText(http.StatusInternalServerError)+err.Error(),
							http.StatusInternalServerError)
						return
					}
					app.SessionManager.Put(request.Context(), "UserJsonData", string(userJson))

					http.Redirect(writer, request, "/", http.StatusSeeOther)
				} else {
					http.Redirect(writer, request, "/register", http.StatusTemporaryRedirect)
				}
			}
		} else {
			http.Error(writer, http.ErrBodyNotAllowed.Error(), http.StatusMethodNotAllowed)
		}
	}
}
