// Package defining handlers for http requests
package handlers

import (
	"errors"
	"fmt"
	"mime"
	"net/http"
	"strconv"
	"strings"

	"github.com/Direct-Dev-Ru/simple-web-app-golang/config"
	db "github.com/Direct-Dev-Ru/simple-web-app-golang/model/db-model"
)

// api/admin/users/

// Обработчик /api/admin/users endpoint for GET
func GetUserHandler(app *config.Application) http.HandlerFunc {

	return func(writer http.ResponseWriter, request *http.Request) {

		writer.Header().Set("Access-Control-Allow-Origin", "*")
		if request.Method == http.MethodOptions {
			return
		}

		if request.Method == http.MethodGet {
			queryValues := request.URL.Query()
			if queryValues.Has("email") {
				if queryEmail := strings.TrimSpace(queryValues.Get("email")); queryEmail != "" {
					user, err := app.AppDbWrap.GetUserByEmail(strings.ToLower(strings.TrimSpace(queryEmail)))

					if err != nil {
						app.RenderErrorJSON(writer, err)
						return
					}
					app.RenderJSON(writer, db.NewUserForClient(user), true)
					return
					// app.RenderJSON(writer, struct {
					// 	Mode string `json:"mode"`
					// 	Echo string `json:"echo"`
					// }{Mode: "email", Echo: queryEmail}, true)
				} else {

					app.RenderErrorJSON(writer, errors.New("empty email"))
					return
				}
			} else if queryValues.Has("id") {
				queryId, err := strconv.Atoi(request.URL.Query().Get("id"))
				if err != nil || queryId < 0 {
					app.RenderErrorJSON(writer, errors.New("wrong id: "+strconv.Itoa(queryId)))
					return
				}

				user, err := app.AppDbWrap.GetUserById(queryId)

				if err != nil {
					app.RenderErrorJSON(writer, err)
					return
				}
				app.RenderJSON(writer, db.NewUserForClient(user), true)
				return

				// app.RenderJSON(writer, struct {
				// 	Mode string `json:"mode"`
				// 	Echo string `json:"echo"`
				// }{Mode: "email", Echo: strconv.Itoa(queryId)}, true)
				// return
			} else {

				// full scan

				users, err := app.AppDbWrap.PopulateUsers()
				if err != nil {
					app.RenderErrorJSON(writer, err)
					return
				}
				sendMap := make(map[string]*db.UserForClient)
				for _, user := range users {
					sendMap[user.Email] = db.NewUserForClient(user)
				}
				app.RenderJSON(writer, sendMap, true)
				return

				// json := struct {
				// 	Mode string `json:"mode"`
				// }{Mode: "full"}
				// app.InfoLog.Println("full get", json)
				// app.RenderJSON(writer, json, true)
				// return
				// app.NotFound(writer)
				// return
			}

		}
	}
}

// Обработчик /api/admin/users endpoint for POST
func CreateUserHandler(app *config.Application) http.HandlerFunc {

	return func(writer http.ResponseWriter, request *http.Request) {

		writer.Header().Set("Access-Control-Allow-Origin", "*")
		if request.Method == http.MethodOptions {
			return
		}

		if request.Method == http.MethodPost {

			// if it is post request for new user creation
			// Types used internally in this handler to (de-)serialize the request and
			// response from/to JSON.
			type RequestUser struct {
				Email      string `json:"email"`
				First_Name string `json:"first_name"`
				Last_Name  string `json:"last_name"`
				// male or female
				Gender   string `json:"gender"`
				Password string `json:"password"`
				// Due time.Time `json:"due"`
			}

			// Enforce a JSON Content-Type.
			contentType := request.Header.Get("Content-Type")
			mediatype, _, err := mime.ParseMediaType(contentType)
			if err != nil {
				http.Error(writer, err.Error(), http.StatusBadRequest)
				return
			}

			if mediatype != "application/json" {
				app.RenderErrorJSON(writer, errors.New("expect application/json Content-Type"))
				// http.Error(writer, "expect application/json Content-Type", http.StatusUnsupportedMediaType)
				return
			}

			dec := app.Json.NewDecoder(request.Body)
			dec.DisallowUnknownFields()
			var reqUser RequestUser
			if err := dec.Decode(&reqUser); err != nil {
				app.RenderErrorJSON(writer, err)
				// http.Error(writer, err.Error(), http.StatusBadRequest)
				return
			}
			// Check if user with such email is presented in the database
			// ***old implementation
			// dbUsers, err := app.AppDbWrap.PopulateUsers()
			// if err != nil {
			// 	app.RenderErrorJSON(writer, err)
			// 	return
			// }
			// existUser, ok := dbUsers[reqUser.Email]
			// ***

			existUser, err := app.AppDbWrap.GetUserByEmail(reqUser.Email)
			if err != nil {
				app.InfoLog.Print(err)
				app.FileLog.Println(err)
				app.RenderErrorJSON(writer, err)
				return
			}
			if existUser != nil {
				// todo: implement update of user
				if strings.TrimSpace(reqUser.First_Name) != "" {
					existUser.First_name = strings.TrimSpace(reqUser.First_Name)
				}
				if strings.TrimSpace(reqUser.Last_Name) != "" {
					existUser.Last_name = strings.TrimSpace(reqUser.Last_Name)
				}
				if strings.TrimSpace(reqUser.Gender) != "" {
					existUser.Gender = strings.TrimSpace(reqUser.Gender)
				}
				if reqUser.Password != "" {
					pwdHash, _ := app.GetPwdHash(reqUser.Password)
					existUser.HashPassword = pwdHash
				}

				fmt.Println(existUser)
				fmt.Fprintf(writer, "Updating user ...")
				return
			}

			pwdHash, _ := app.GetPwdHash(reqUser.Password)
			var newUser *db.User = db.NewUser(reqUser.Email, reqUser.First_Name, reqUser.Last_Name, "", reqUser.Gender, pwdHash)

			// app.InfoLog.Println(newUser)
			newUsersMap, err := app.AppDbWrap.AppendUsers(newUser)
			if err != nil {
				app.InfoLog.Print(err)
				app.FileLog.Println(err)
				app.RenderErrorJSON(writer, err)
				// http.Error(writer, err.Error(), http.StatusInternalServerError)
				return
			}
			// for _, v := range newUsersMap {
			// 	app.InfoLog.Println(*v)
			// }
			app.RenderJSON(writer, newUsersMap, true)
		}
	}
}
