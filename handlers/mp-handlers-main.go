// Package defining handlers for http requests
package handlers

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/Direct-Dev-Ru/simple-web-app-golang/config"
	db_model "github.com/Direct-Dev-Ru/simple-web-app-golang/model/db-model"
	mylog "github.com/Direct-Dev-Ru/simple-web-app-golang/utils/logs"
	"github.com/justinas/nosurf"
)

// Root handler
func WelcomeHandler(app *config.Application) http.HandlerFunc {

	return func(writer http.ResponseWriter, request *http.Request) {

		// if request.URL.Path != "/" {
		// 	http.NotFound(writer, request)
		// 	return
		// }
		// app.InfoLog.Println("is_auth", request.Context().Value(config.MyKeyString("IsAuthenticated")))

		var isAuthenticated bool = false
		var userName string = ""
		// var userEmail string = ""

		if isAuth := request.Context().Value(config.MyKeyString("IsAuthenticated")).(bool); isAuth {
			isAuthenticated = true
			userName = request.Context().Value(config.MyKeyString("UserName")).(string)
			// userEmail = request.Context().Value(config.MyKeyString("UserEmail")).(string)
		}

		// app.InfoLog.Println(app.SessionManager.GetString(request.Context(), "UserJsonData"))

		err := TemplatesCache["home.page"].Execute(writer, struct {
			Info            *db_model.Info
			IsAuthenticated bool
			UserName        string
		}{app.Store.Info, isAuthenticated, userName})
		if err != nil {
			app.Logger.ErrorPrintln("Error while processing template home.page", err)
			http.Error(writer, http.StatusText(http.StatusInternalServerError),
				http.StatusInternalServerError)
			return
		}
	}
}

// обработчик вывода списка проголосовавших

func ListHandler(app *config.Application) http.HandlerFunc {

	return func(writer http.ResponseWriter, request *http.Request) {
		answer, err := strconv.Atoi(request.URL.Query().Get("answer"))
		if err != nil || !(answer == -1 || answer == 1) {
			http.NotFound(writer, request)
			return
		}
		strAnswer := "YESSS"
		if answer == -1 {
			strAnswer = "NOOOO"
		}
		templateData := struct {
			Results      []*db_model.Member
			ResultAnswer string
			FilterValue  int
		}{
			Results:      app.Store.Members,
			ResultAnswer: strAnswer,
			FilterValue:  answer,
		}
		err = TemplatesCache["list.page"].Execute(writer, templateData)
		if err != nil {
			mylog.AppLogger.ErrorPrintln("Error while processing template list.page", templateData, err)
			http.Error(writer, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
	}

}

// структура для формы - композиция из Members и среза ошибок - для обмена с формой клиента
type formData struct {
	*db_model.Member
	*db_model.Info
	Errors []string
	Token  string
}

// Обработчик страницы с формой
func FormHandler(app *config.Application) http.HandlerFunc {

	return func(writer http.ResponseWriter, request *http.Request) {

		// CORS allow for *
		writer.Header().Set("Access-Control-Allow-Origin", "*")
		if request.Method == http.MethodOptions {
			return
		}
		// if not isAuth - go to home page
		if isAuth := request.Context().Value(config.MyKeyString("IsAuthenticated")).(bool); !isAuth {
			http.Redirect(writer, request, "/", http.StatusSeeOther)
		}

		// lets get user from database
		userEmail := request.Context().Value(config.MyKeyString("UserEmail")).(string)

		memberData, err := app.Store.GetMemberByEmail(userEmail)
		if err != nil {
			http.Error(writer, http.StatusText(http.StatusInternalServerError)+" "+err.Error(), http.StatusInternalServerError)
			return
		}

		if memberData == nil {
			memberData := &db_model.Member{}
			user, err := app.AppDbWrap.GetUserByEmail(userEmail)
			if err == nil && user != nil {
				memberData.Email = user.Email
				memberData.FirstName = user.First_name
				memberData.LastName = user.Last_name
			}
		}
		csrfToken := nosurf.Token(request)

		// Method processing
		if request.Method == http.MethodGet {

			TemplatesCache["form.page"].Execute(writer, formData{
				Member: memberData,
				Info:   app.Store.Info,
				Errors: []string{},
				Token:  csrfToken,
			})
		} else if request.Method == http.MethodPost {
			// если идет post запрос от страницы с формой то разобрать ее

			request.ParseForm()
			// собрать в структуру Member, что пришло от клиента - [0] так как на форме могут быть несколько
			// элементов с одинаковым name атрибутом
			responseData := db_model.Member{
				Id:        -1000,
				FirstName: strings.TrimSpace(request.Form["fname"][0]),
				LastName:  strings.TrimSpace(request.Form["lname"][0]),
				Email:     strings.TrimSpace(request.Form["email"][0]),
				Phone:     strings.TrimSpace(request.Form["phone"][0]),
				Comment:   strings.TrimSpace(request.Form["comment"][0]),
			}
			if request.Form["vote"][0] == "true" {
				responseData.Vote = 1
			} else {
				responseData.Vote = -1
			}

			// Check errors
			errors := []string{}
			if responseData.FirstName == "" {
				errors = append(errors, "Please enter your first name")
			}
			if responseData.LastName == "" {
				errors = append(errors, "Please enter your last name")
			}
			if responseData.Email == "" {
				errors = append(errors, "Please enter your email address")
			}
			if responseData.Email != userEmail {
				errors = append(errors, "Email must be yours ("+userEmail+")")
			}
			if responseData.Phone == "" {
				errors = append(errors, "Please enter your phone number")
			}
			// I errors - we senf to client fields data plus errors

			if len(errors) > 0 {
				csrfToken := nosurf.Token(request)
				TemplatesCache["form.page"].Execute(writer, formData{
					Member: &responseData,
					Errors: errors,
					Info:   app.Store.Info,
					Token:  csrfToken,
				})
			} else {
				m, err := app.Store.GetMemberByEmail(responseData.Email)

				if err != nil {
					http.Error(writer, http.StatusText(http.StatusInternalServerError)+" "+err.Error(), http.StatusInternalServerError)
					return
				}
				if m != nil {
					// Member with such email exists in database
					err = app.Store.UpdateMember(&responseData, true)
				} else {
					err = app.Store.AppendMember(&responseData, true)
				}

				if err == nil {
					if responseData.Vote == 1 {
						TemplatesCache["thanks.page"].Execute(writer, responseData.FirstName+" "+responseData.LastName)
					} else {
						TemplatesCache["sorry.page"].Execute(writer, responseData.FirstName+" "+responseData.LastName)
					}
				} else {
					http.Error(writer, http.StatusText(http.StatusInternalServerError)+
						" "+err.Error(), http.StatusInternalServerError)
					return
				}
			}
		}
	}
}

// функция загрузки шаблонов с диска
// func LoadTemplates() {
// 	templateNames := [5]string{"welcome", "form", "thanks", "sorry", "list"}
// 	for index, name := range templateNames {
// 		t, err := template.ParseFiles("./templates/layout.html", "./templates/"+name+".html")
// 		if err == nil {
// 			Templates[name] = t
// 			fmt.Println("Loaded template", index, name)
// 		} else {
// 			panic(err)
// 		}
// 	}
// }
