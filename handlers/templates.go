// Package defining handlers for http requests
package handlers

import (
	"html/template"
	"path/filepath"
	"strings"

	app "github.com/Direct-Dev-Ru/simple-web-app-golang/config"
	mylog "github.com/Direct-Dev-Ru/simple-web-app-golang/utils/logs"
)

// карта шаблонов - содержит по текстовому ключу ссылки на шаблоны
var TemplatesCache = make(map[string]*template.Template, 10)

var _, errorLog, _ = mylog.AppLogger.GetAllLogs()

func LoadTemplatesCache(dir string, app *app.Application) (*map[string]*template.Template, error) {
	cache := &map[string]*template.Template{}
	if dir == "" {
		dir = app.TemplatesPath
	}
	pages, err := filepath.Glob(filepath.Join(dir, "*.page.html"))
	if err != nil {
		return nil, err
	}

	for index, page := range pages {
		basename := filepath.Base(page)

		filename := strings.TrimSuffix(basename, filepath.Ext(basename))

		ts, err := template.ParseFiles(page)
		if err != nil {
			return nil, err
		}

		// Используем метод ParseGlob для добавления всех каркасных шаблонов.
		// В нашем случае это только файл base.layout.tmpl (основная структура шаблона).
		ts, err = ts.ParseGlob(filepath.Join(dir, "*.layout.html"))
		if err != nil {
			return nil, err
		}

		// Используем метод ParseGlob для добавления всех вспомогательных шаблонов.
		// В нашем случае это footer.partial.tmpl "подвал" нашего шаблона.
		ts, err = ts.ParseGlob(filepath.Join(dir, "partial", "*.partial.html"))
		if err != nil {
			return nil, err
		}
		if err == nil {
			TemplatesCache[filename] = ts
			// infoLog.Println("Loaded new template №", (index + 1), name)
			mylog.AppLogger.InfoPrintln("Successfully loaded new template №", (index + 1), filename)
		} else {
			mylog.AppLogger.ErrorPrintln("Error while loading new template №", (index + 1), filename)
			errorLog.Fatal(err)
		}
		// Добавляем полученный набор шаблонов в кэш, используя название страницы
		// (например, home.page) в качестве ключа для нашей карты.
		(*cache)[filename] = ts

	}

	// Возвращаем полученную карту.
	return cache, nil

	// templateNames := []string{"home.page", "form.page", "list.page", "sorry.page", "thanks.page"}
	// for index, name := range templateNames {
	// 	files := []string{
	// 		"./ui/templates/" + name + ".html",
	// 		"./ui/templates/base.layout.html",
	// 		"./ui/templates/partial/footer.partial.html",
	// 	}
	// 	t, err := template.ParseFiles(files...)
	// 	if err == nil {
	// 		TemplatesCache[name] = t
	// 		// infoLog.Println("Loaded new template №", (index + 1), name)
	// 		mylog.AppLogger.InfoPrintln("Successfully loaded new template №", (index + 1), name)
	// 	} else {
	// 		mylog.AppLogger.ErrorPrintln("Error while loading new template №", (index + 1), name)
	// 		errorLog.Fatal(err)
	// 	}
	// }
}
