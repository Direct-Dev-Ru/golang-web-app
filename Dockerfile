FROM golang:alpine@sha256:d84b1ff3eeb9404e0a7dda7fdc6914cbe657102420529beec62ccb3ef3d143eb
RUN mkdir /webapp
WORKDIR /webapp
COPY go.mod .
RUN go mod download
COPY . .
RUN go build -o /webapp/cmd/web/webapp /webapp/cmd/web/main.go
ENTRYPOINT ["/webapp/cmd/web/webapp"]