package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"sync"
	"time"
)

type Member struct {
	Id        int       `json:"id"`
	FirstName string    `json:"firstName"`
	LastName  string    `json:"lastName"`
	Email     string    `json:"email"`
	Phone     string    `json:"phone"`
	Comment   string    `json:"comment"`
	Vote      int       `json:"vote"`
	CreatedAt time.Time `json:"created_at"`
}

type MemberAlt struct {
	FirstName string    `json:"firstName"`
	LastName  string    `json:"lastName"`
	Email     string    `json:"email"`
	Phone     string    `json:"phone"`
	Comment   string    `json:"comment"`
	Vote      int       `json:"vote"`
	CreatedAt time.Time `json:"created_at"`
}

type Info struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

type Store struct {
	Info    *Info
	Members []*Member
	sync.Mutex
	StorePath *string
}

type Databaser interface {
	// GetDbPool() *sql.DB
	GetAppLoggers() (*log.Logger, *log.Logger, *log.Logger)
	GetStorePath() *string
}

type Application struct {
	StorePath string
}

func (app Application) GetAppLoggers() (*log.Logger, *log.Logger, *log.Logger) {
	return log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime),
		log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Llongfile),
		nil
}

func (app Application) GetStorePath() *string {
	defaultStorePath := "./data/test-list.json"
	return &defaultStorePath
}

func logError(err error, app Databaser) {
	if err != nil {
		i, e, f := app.GetAppLoggers()
		if i != nil {
			i.Println(err)
		}
		if e != nil {
			e.Println(err)
		}
		if f != nil {
			f.Println(err)
		}
	}
}

func NewStore(app Databaser) *Store {

	defaultStorePath := "./data/test-list.json"
	resultStorePath := defaultStorePath
	if *app.GetStorePath() != "" {
		resultStorePath = *app.GetStorePath()
	}

	var store Store
	store.Lock()
	defer store.Unlock()
	file, err := os.OpenFile(resultStorePath, os.O_RDONLY, 0666)
	logError(err, app)
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	logError(err, app)

	json.Unmarshal(b, &store)
	store.StorePath = app.GetStorePath()

	return &store
}

func (store *Store) PrintBdList() {
	var json_data, err = json.MarshalIndent(store, "", "  ")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(json_data))
}

func (store *Store) AppendMember(m *Member, save bool) (err error) {
	//appending newMember to slice of all Members and rewrite json file
	// backupSlice := make([]*scheme.Member, len(store.Members))
	// copy(backupSlice, store.Members)
	store.Lock()
	defer store.Unlock()
	store.Members = append(store.Members, m)

	if save {
		err := store.Save()
		return err
	}

	return nil
}

func (store *Store) Save() (err error) {

	store.Lock()
	defer store.Unlock()
	newMembersBytes, err := json.MarshalIndent(store, "", "  ")
	if err != nil {
		return
	}
	err = ioutil.WriteFile(*store.StorePath, newMembersBytes, 0666)
	if err != nil {
		return
	}

	return nil
}

func (store *Store) UpdateMember(mUpdate interface{}, field string, save bool) (err error) {
	//appending newMember to slice of all Members and rewrite json file
	// backupSlice := make([]*scheme.Member, len(store.Members))
	// copy(backupSlice, store.Members)
	store.Lock()
	defer store.Unlock()
	for _, member := range store.Members {
		tMUp := reflect.TypeOf(mUpdate)
		vMUp := reflect.ValueOf(mUpdate)

		tMember := reflect.TypeOf(*member)
		vMember := reflect.ValueOf(*member)
		update := false

		for i := 0; i < tMUp.NumField(); i++ {
			tMUpFieldName := tMUp.Field(i).Name
			tMUpFieldType := tMUp.Field(i).Type
			tMemberF, ok1 := tMember.FieldByName(tMUpFieldName)
			_, ok2 := tMUp.FieldByName(tMUpFieldName)

			if tMUpFieldName == field && ok1 && ok2 {
				tMemberFieldType := tMemberF.Type

				if tMUpFieldType.Name() == tMemberFieldType.Name() {
					switch tMUpFieldType.Name() {
					case "string":
						db_m_v := vMember.FieldByName(tMUpFieldName).String()
						new_m_v := vMUp.FieldByName(tMUpFieldName).String()
						fmt.Println(db_m_v, new_m_v)
						update = db_m_v == new_m_v

					case "int":
						db_m_v := vMember.FieldByName(tMUpFieldName).Int()
						new_m_v := vMUp.FieldByName(tMUpFieldName).Int()
						fmt.Println(db_m_v, new_m_v)
						update = db_m_v == new_m_v
					}
				}
			}
			if update {
				break
			}
		}

		if update {
			mutable := reflect.ValueOf(member).Elem()
			for i := 0; i < tMUp.NumField(); i++ {
				tMUpFieldName := tMUp.Field(i).Name
				tMUpFieldType := tMUp.Field(i).Type
				tMemberF, ok1 := tMember.FieldByName(tMUpFieldName)
				_, ok2 := tMUp.FieldByName(tMUpFieldName)

				if ok1 && ok2 {
					tMemberFieldType := tMemberF.Type
					fmt.Println(tMUpFieldName, tMUpFieldType.Name(), tMemberFieldType.Name())
					if tMUpFieldType.Name() == tMemberFieldType.Name() {
						switch tMUpFieldType.Name() {
						case "string":

							if mutable.FieldByName(tMUpFieldName).CanSet() {
								new_m_v := vMUp.FieldByName(tMUpFieldName).String()
								mutable.FieldByName(tMUpFieldName).SetString(new_m_v)
							}

						case "int":
							if mutable.FieldByName(tMUpFieldName).CanSet() {
								new_m_v := vMUp.FieldByName(tMUpFieldName).Int()
								mutable.FieldByName(tMUpFieldName).SetInt(new_m_v)
							}

						case "Time":
							if mutable.FieldByName(tMUpFieldName).CanSet() {
								new_m_v := vMUp.FieldByName(tMUpFieldName)
								fmt.Println(new_m_v)
								mutable.FieldByName(tMUpFieldName).Set(new_m_v)
							}
						}
					}
				}

			}
		}
	}

	if save {
		return store.Save()
	}

	return nil
}

type FSProvider struct {
	filePath string
}

func NewFSProvider(fpath string) FSProvider {
	var fs FSProvider = FSProvider{filePath: fpath}
	return fs
}

func (provider FSProvider) SelectAllMembers() (map[string]*Member, error) {

	if provider.filePath == "" {
		return nil, errors.New("empty filePath in fs-provider")
	}

	var store Store
	file, err := os.Open(provider.filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&store)
	if err != nil {
		return nil, err
	}
	// store.PrintBdList()
	mmap := make(map[string]*Member, len(store.Members))
	for _, m := range store.Members {
		mmap[m.Email] = m
	}
	return mmap, nil
}

func main() {
	// app := Application{}
	// testStore := NewStore(app)

	// m1 := &Member{Id: 1, FirstName: "Anton", LastName: "Smith", Email: "info@direct-dev.ru",
	// 	Phone: "111222", Vote: 1, Comment: "some comment", CreatedAt: time.Now()}

	// mUpd := &MemberAlt{FirstName: "Anton", LastName: "Smith", Email: "info@direct-dev.ru",
	// 	Phone: "333222", Vote: 1, Comment: "some new comment", CreatedAt: time.Now().Add(time.Hour * 24)}

	// testStore.PrintBdList()
	// testStore.AppendMember(m1, false)

	// fmt.Println("-----------------------------------")

	// m1.Comment = "comment m1"
	// testStore.UpdateMember(*mUpd, "Email", false)

	// fmt.Println("-----------------------------------")
	// testStore.PrintBdList()
	// fmt.Println("-----------------------------------")

	fs := NewFSProvider("./data/test-list.json")
	mmap, err := fs.SelectAllMembers()
	fmt.Println(mmap, err)
}
