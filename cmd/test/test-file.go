package main

import (
	"log"
	"os"
)

var (
	myfile *os.FileInfo
	e      error
)

func main() {

	// Here Stat() function returns file info and
	//if there is no file, then it will return an error

	myfile, e := os.Stat("./data/test-list.json")
	if e != nil {

		// Checking if the given file exists or not
		// Using IsNotExist() function
		if os.IsNotExist(e) {
			log.Fatal("File not Found !!")
		}
	}
	log.Println("File Exist!!")
	log.Println("Detail of file is:")
	log.Println("Name: ", myfile.Name())
	log.Println("Size: ", myfile.Size())

}
