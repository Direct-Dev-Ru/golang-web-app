package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/Direct-Dev-Ru/simple-web-app-golang/config"
	"github.com/Direct-Dev-Ru/simple-web-app-golang/handlers"
	db "github.com/Direct-Dev-Ru/simple-web-app-golang/model/db-model"
	mylog "github.com/Direct-Dev-Ru/simple-web-app-golang/utils/logs"
	gorilla_handlers "github.com/gorilla/handlers"
	gorilla_mux "github.com/gorilla/mux"
)

type neuteredFileSystem struct {
	fs http.FileSystem
}

func (nfs neuteredFileSystem) Open(path string) (http.File, error) {
	f, err := nfs.fs.Open(path)
	if err != nil {
		return nil, err
	}

	s, _ := f.Stat()
	if s.IsDir() {
		index := filepath.Join(path, "index.html")
		if _, err := nfs.fs.Open(index); err != nil {
			closeErr := f.Close()
			if closeErr != nil {
				return nil, closeErr
			}

			return nil, err
		}
	}

	return f, nil
}

func main() {

	// init logs
	flog, err := mylog.AppLogger.SetFileLog("app.log")
	_, errorLog, _ := mylog.AppLogger.GetAllLogs()
	if err != nil {
		errorLog.Println(err)
	}
	defer flog.Close()

	addr := flag.String("addr", ":5000", "Network address of web server")
	storePath := flag.String("store", "./data/store.json", "Path to JSON file")
	// dsn := flag.String("dsn", "mysqluser:userpassword@tcp(127.0.0.1:3306)/db?parseTime=true", "Connection string to mysql database")
	dsn := flag.String("dsn", "mysqluser:userpassword@tcp(192.168.88.204:3306)/db?parseTime=true", "Connection string to mysql database")
	flag.Parse()
	// fmt.Println("App Store Path:", *storePath)

	app := config.NewApplication(storePath)

	// fakePath := "./data/store.json"
	// app2 := config.NewApplication(&fakePath)
	// newstore := db.NewStore(app2)
	// newstore.PrintBdList()

	mux := http.NewServeMux()

	app.TemplatesCache, err = handlers.LoadTemplatesCache("", app)
	if err != nil {
		errorLog.Fatal(err)
	}

	// db open
	database, err := app.OpenDB(*dsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer database.Close()
	app.AppDbWrap = db.NewDbWrap(*app)
	app.SetStore(db.NewStore(app))

	mux.HandleFunc("/", handlers.WelcomeHandler(app))
	mux.HandleFunc("/list", handlers.ListHandler(app))
	mux.HandleFunc("/form", handlers.FormHandler(app))

	fileServer := http.FileServer(neuteredFileSystem{http.Dir("./ui/static")})
	mux.Handle("/static", http.NotFoundHandler())
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))

	router := gorilla_mux.NewRouter()
	router.StrictSlash(true)
	router.HandleFunc("/", handlers.WelcomeHandler(app)).Methods(http.MethodGet)
	router.HandleFunc("/login/", handlers.LogInHandler(app)).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	router.HandleFunc("/register/", handlers.SignUpHandler(app)).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	router.HandleFunc("/list/", handlers.ListHandler(app)).Methods(http.MethodGet)
	// IMPORTANT: you must specify an OPTIONS method matcher for the middleware to set CORS headers
	router.HandleFunc("/form/", handlers.FormHandler(app)).Methods(http.MethodGet, http.MethodPost, http.MethodOptions)
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fileServer))

	router.HandleFunc("/api/health/", app.HealthCheckHandler).Methods(http.MethodGet)
	router.HandleFunc("/api/admin/users/", handlers.GetUserHandler(app)).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc("/api/admin/users/", handlers.CreateUserHandler(app)).Methods(http.MethodPost, http.MethodOptions)
	// router.HandleFunc("/task/", server.deleteAllTasksHandler).Methods("DELETE")
	// router.HandleFunc("/task/{id:[0-9]+}/", server.getTaskHandler).Methods("GET")
	// router.HandleFunc("/task/{id:[0-9]+}/", server.deleteTaskHandler).Methods("DELETE")
	// router.HandleFunc("/tag/{tag}/", server.tagHandler).Methods("GET")
	// router.HandleFunc("/due/{year:[0-9]+}/{month:[0-9]+}/{day:[0-9]+}/", server.dueHandler).Methods("GET")

	// router.Use(app.Logging)
	// Set up logging and panic recovery middleware.
	router.Use(func(h http.Handler) http.Handler {
		return gorilla_handlers.LoggingHandler(os.Stdout, h)
	})
	router.Use(gorilla_handlers.RecoveryHandler(gorilla_handlers.PrintRecoveryStack(true)))

	router.Use(app.SessionManager.LoadAndSave)
	router.Use(app.NoSurf)
	router.Use(app.IsAuthenticated)

	router.Use(gorilla_mux.CORSMethodMiddleware(router))

	srv := &http.Server{
		Handler:           router,
		Addr:              *addr,
		ErrorLog:          errorLog,
		ReadHeaderTimeout: time.Second * 10,
		WriteTimeout:      time.Second * 10,
	}

	app.InfoLog.Info().Msg(fmt.Sprintf("Web https server is now running on %s", *addr))
	srv_err := srv.ListenAndServeTLS("./cert/golang-webapp.dev+3.pem",
		"./cert/golang-webapp.dev+3-key.pem")
	if srv_err != nil {
		errorLog.Fatal(srv_err)
	}
}
